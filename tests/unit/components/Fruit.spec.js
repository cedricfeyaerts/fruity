import Fruit from '@/components/Fruit.vue';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

let localVue;

const componentFactory = (propsData = {}) => {
  return shallowMount(Fruit, { propsData });
};

const fruit = {
  isFruit: true,
  name: 'a fruit',
  taste: 'fruity',
  description: 'like a banana or an apple but better',
  price: '5',
  color: '#ffaa00',
  image: 'https://an_image_url.com',
};

describe('Fruit', () => {
  it('show the name', () => {
    const wrapper = componentFactory({ fruit: fruit });

    expect(wrapper.find('.fruit__name').text()).toEqual(fruit.name);
  });

  it('show the taste', () => {
    const wrapper = componentFactory({ fruit: fruit });

    expect(wrapper.find('.fruit__taste').text()).toEqual(fruit.taste);
  });

  it('show the description', () => {
    const wrapper = componentFactory({ fruit: fruit });

    expect(wrapper.find('.fruit__description').text()).toEqual(
      fruit.description
    );
  });

  it('show the price with currency', () => {
    const wrapper = componentFactory({ fruit: fruit });

    expect(wrapper.find('.fruit__price').text()).toEqual('$' + fruit.price);
  });

  it('show the image', () => {
    const wrapper = componentFactory({ fruit: fruit });

    expect(wrapper.find('.fruit__header').attributes('style')).toMatch(
      fruit.image
    );
  });

  it('has the fruit color', () => {
    const wrapper = componentFactory({ fruit: fruit });

    expect(wrapper.find('.fruit').attributes('style')).toMatch(
      'rgb(255, 170, 0);'
    );
  });

  it('has a random color if none is provided', () => {
    const wrapper = componentFactory({ fruit: { ...fruit, color: null } });

    expect(wrapper.find('.fruit').attributes('style')).not.toMatch(
      'rgb(255, 170, 0);'
    );
    expect(wrapper.find('.fruit').attributes('style')).toMatch(
      /rgb\([0-9]+, [0-9]+, [0-9]+\)/
    );
  });

  it('dispatch the deleteFruit action when click on delete button', () => {
    const actionsMock = {
      deleteFruit: jest.fn(),
    };
    localVue = createLocalVue();
    localVue.use(Vuex);
    const store = new Vuex.Store({
      state: {},
      getters: {},
      actions: actionsMock,
      mutations: {},
    });

    let wrapper = shallowMount(Fruit, {
      store,
      localVue,
      propsData: { fruit },
    });
    wrapper.find('.fruit__delete-btn').trigger('click');

    expect(actionsMock.deleteFruit).toHaveBeenCalledTimes(1);
  });
});
