import { shallowMount } from '@vue/test-utils';
import Btn from '@/components/Btn.vue';

describe('Btn', () => {
  it('has a label in the title', () => {
    const wrapper = shallowMount(Btn, {
      propsData: { label: 'this is a label' },
    });

    expect(wrapper.find('a').attributes('title')).toEqual('this is a label');
  });
});
