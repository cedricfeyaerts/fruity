import { shallowMount } from '@vue/test-utils';
import Textfield from '@/components/Textfield.vue';

describe('Textfield', () => {
  it('shows the label', () => {
    const wrapper = shallowMount(Textfield, {
      propsData: { label: 'this is a label' },
    });

    expect(wrapper.find('label').text()).toEqual('this is a label');
  });

  it('shows the placeholder', () => {
    const wrapper = shallowMount(Textfield, {
      propsData: { placeholder: 'this is a placeholder' },
    });

    expect(wrapper.find('input').attributes('placeholder')).toEqual(
      'this is a placeholder'
    );
  });

  it('has the correct type', () => {
    const wrapper = shallowMount(Textfield, {
      propsData: { type: 'number' },
    });

    expect(wrapper.find('input').attributes('type')).toEqual('number');
  });

  it('receive input', async () => {
    const wrapper = shallowMount(Textfield, {
      propsData: { type: 'text' },
    });

    const input = wrapper.find('input');
    await input.setValue('some value');

    expect(wrapper.emitted().input[0]).toEqual(['some value']);
  });
});
