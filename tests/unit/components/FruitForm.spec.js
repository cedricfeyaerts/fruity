import FruitForm from '@/components/FruitForm.vue';
import Btn from '@/components/Btn.vue';
import Textfield from '@/components/Textfield.vue';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import getters from '@/store/getters.js';
import Vuex from 'vuex';

let storeConfig;
let actionsMock;
let mutationsMock;

const componentFactory = (state = {}) => {
  actionsMock = {
    createFruit: jest.fn(),
  };
  mutationsMock = {
    updateNewFruit: jest.fn(),
  };
  storeConfig = {
    state,
    mutations: mutationsMock,
    actions: actionsMock,
    getters,
  };
  let localVue = createLocalVue();
  localVue.use(Vuex);
  let store = new Vuex.Store(storeConfig);
  return shallowMount(FruitForm, { store, localVue });
};

const state = {
  newFruit: {
    isFruit: true,
    name: null,
    taste: null,
    description: null,
    price: null,
    color: null,
    image: null,
  },
};

describe('FruitForm', () => {
  describe('Btn Add', () => {
    it('has the label Add', () => {
      const wrapper = componentFactory(state);

      expect(wrapper.findAllComponents(Btn).at(0).props('label')).toEqual(
        'Add'
      );
    });
    it('opens the form when clicked', async () => {
      const wrapper = componentFactory(state);
      expect(wrapper.classes()).not.toContain('fruit-form--open');
      await wrapper.findAllComponents(Btn).at(0).trigger('click');
      expect(wrapper.classes()).toContain('fruit-form--open');
    });
  });

  describe('Btn Submit', () => {
    it('has the label Submit', () => {
      const wrapper = componentFactory(state);

      expect(wrapper.findAllComponents(Btn).at(1).props('label')).toEqual(
        'Submit'
      );
    });
    it('send the form when clicked', async () => {
      const wrapper = componentFactory(state);
      expect(wrapper.classes()).not.toContain('fruit-form--open');
      await wrapper.findAllComponents(Btn).at(1).trigger('click');
      expect(actionsMock.createFruit).toHaveBeenCalledTimes(1);
    });
  });

  describe('Btn Cancel', () => {
    it('has the label Cancel', () => {
      const wrapper = componentFactory(state);

      expect(wrapper.findAllComponents(Btn).at(2).props('label')).toEqual(
        'Cancel'
      );
    });
    it('close the form when clicked', async () => {
      const wrapper = componentFactory(state);
      await wrapper.findAllComponents(Btn).at(0).trigger('click');
      expect(wrapper.classes()).toContain('fruit-form--open');
      await wrapper.findAllComponents(Btn).at(2).trigger('click');
      expect(wrapper.classes()).not.toContain('fruit-form--open');
    });
  });

  describe('textField Name', () => {
    it('has value of newFruit.name', () => {
      const state = {
        newFruit: {
          name: 'some name',
        },
      };
      const wrapper = componentFactory(state);

      expect(wrapper.findAllComponents(Textfield).at(0).props('value')).toEqual(
        'some name'
      );
    });

    it('commit updateNewFruit on input', async () => {
      const wrapper = componentFactory(state);

      await wrapper
        .findAllComponents(Textfield)
        .at(0)
        .vm.$emit('input', 'some name');
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledTimes(1);
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledWith(state, {
        field: 'name',
        value: 'some name',
      });
    });
  });

  describe('textField Taste', () => {
    it('has value of newFruit.taste', () => {
      const state = {
        newFruit: {
          taste: 'some taste',
        },
      };
      const wrapper = componentFactory(state);

      expect(wrapper.findAllComponents(Textfield).at(1).props('value')).toEqual(
        'some taste'
      );
    });

    it('commit updateNewFruit on input', async () => {
      const wrapper = componentFactory(state);

      await wrapper
        .findAllComponents(Textfield)
        .at(1)
        .vm.$emit('input', 'some taste');
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledTimes(1);
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledWith(state, {
        field: 'taste',
        value: 'some taste',
      });
    });
  });

  describe('textField Price', () => {
    it('has value of newFruit.price', () => {
      const state = {
        newFruit: {
          price: 'some price',
        },
      };
      const wrapper = componentFactory(state);

      expect(wrapper.findAllComponents(Textfield).at(2).props('value')).toEqual(
        'some price'
      );
    });

    it('commit updateNewFruit on input', async () => {
      const wrapper = componentFactory(state);

      await wrapper
        .findAllComponents(Textfield)
        .at(2)
        .vm.$emit('input', 'some price');
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledTimes(1);
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledWith(state, {
        field: 'price',
        value: 'some price',
      });
    });
  });

  describe('textField Description', () => {
    it('has value of newFruit.description', () => {
      const state = {
        newFruit: {
          description: 'some description',
        },
      };
      const wrapper = componentFactory(state);

      expect(wrapper.findAllComponents(Textfield).at(3).props('value')).toEqual(
        'some description'
      );
    });

    it('commit updateNewFruit on input', async () => {
      const wrapper = componentFactory(state);

      await wrapper
        .findAllComponents(Textfield)
        .at(3)
        .vm.$emit('input', 'some description');
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledTimes(1);
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledWith(state, {
        field: 'description',
        value: 'some description',
      });
    });
  });

  describe('textField Image', () => {
    it('has value of newFruit.image', () => {
      const state = {
        newFruit: {
          image: 'some image',
        },
      };
      const wrapper = componentFactory(state);

      expect(wrapper.findAllComponents(Textfield).at(4).props('value')).toEqual(
        'some image'
      );
    });

    it('commit updateNewFruit on input', async () => {
      const wrapper = componentFactory(state);

      await wrapper
        .findAllComponents(Textfield)
        .at(4)
        .vm.$emit('input', 'some image');
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledTimes(1);
      expect(mutationsMock.updateNewFruit).toHaveBeenCalledWith(state, {
        field: 'image',
        value: 'some image',
      });
    });
  });
});
