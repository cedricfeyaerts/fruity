import App from '@/App.vue';
import Header from '@/components/Header.vue';
import Fruit from '@/components/Fruit.vue';
import Footer from '@/components/Footer.vue';
import Spinner from '@/components/Spinner.vue';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import mutations from '@/store/mutations.js';
import getters from '@/store/getters.js';
import Vuex from 'vuex';

let storeConfig;
let actionsMock;

const componentFactory = (state = {}) => {
  actionsMock = {
    loadData: jest.fn(),
  };
  storeConfig = { state, mutations, actions: actionsMock, getters };
  let localVue = createLocalVue();
  localVue.use(Vuex);
  let store = new Vuex.Store(storeConfig);
  return shallowMount(App, { store, localVue });
};

const state = {
  fruitById: {
    1: {
      isFruit: true,
      name: 'a fruit',
      taste: 'fruity',
      description: 'like a banana or an apple but better',
      price: '5',
      color: '#ffaa00',
      image: 'https://an_image_url.com',
    },
  },
};

describe('App', () => {
  it('has a header', () => {
    const wrapper = componentFactory(state);

    expect(wrapper.findComponent(Header).exists()).toBe(true);
  });

  it('has a footer', () => {
    const wrapper = componentFactory(state);

    expect(wrapper.findComponent(Footer).exists()).toBe(true);
  });

  it('has a fruit if fruitById contain a fruit', () => {
    const wrapper = componentFactory(state);

    expect(wrapper.findComponent(Fruit).exists()).toBe(true);
  });

  it('has no a fruit if fruitById is empty', () => {
    const wrapper = componentFactory({ ...state, fruitById: {} });

    expect(wrapper.findComponent(Fruit).exists()).toBe(false);
  });

  it('has a spinner if loading', () => {
    const wrapper = componentFactory({ ...state, loading: true });

    expect(wrapper.findComponent(Spinner).exists()).toBe(true);
  });

  it('has no a spinner if not loading', () => {
    const wrapper = componentFactory({ ...state, loading: false });

    expect(wrapper.findComponent(Spinner).exists()).toBe(false);
  });

  it('it dispactch loadData action ', () => {
    componentFactory({ ...state, loading: false });

    expect(actionsMock.loadData).toHaveBeenCalledTimes(1);
  });
});
