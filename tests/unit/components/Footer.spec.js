import { shallowMount } from '@vue/test-utils';
import Footer from '@/components/Footer.vue';
import FruitForm from '@/components/FruitForm.vue';

describe('Footer', () => {
  it('has a form component', () => {
    const wrapper = shallowMount(Footer);

    expect(wrapper.findComponent(FruitForm).exists()).toBe(true);
  });
});
