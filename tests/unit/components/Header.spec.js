import { shallowMount } from '@vue/test-utils';
import Header from '@/components/Header.vue';

describe('Header', () => {
  it('shows the title', () => {
    const wrapper = shallowMount(Header);

    expect(wrapper.find('h1').text()).toEqual('Fruity');
  });
});
