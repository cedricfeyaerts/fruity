import { extractFruits } from '@/utils/fruit-api.js';

describe('extractFruits', () => {
  it('extract valid fruit object from data', () => {
    const data = {
      isFruit: true,
      name: 'apple',
      image: 'image',
      price: 'price',
      color: 'color',
      description: 'description',
      taste: 'taste',
      expires: '2020-07-10T21:42:38.596Z',
      id: 1,
    };
    const fruits = extractFruits(data);
    expect(fruits).toEqual([
      {
        isFruit: true,
        name: 'apple',
        image: 'image',
        price: 'price',
        color: 'color',
        description: 'description',
        taste: 'taste',
        expires: '2020-07-10T21:42:38.596Z',
        id: 1,
      },
    ]);
  });

  it('extract fruit if there is at least isFruit, a name and an id', () => {
    const data = {
      fruit: {
        isFruit: true,
        name: 'apple',
        id: 1,
      },
      notafruit1: {
        name: 'notafruit1',
        id: 1,
      },
      notafruit2: {
        isFruit: true,
        id: 1,
      },
      notafruit3: {
        isFruit: true,
        name: 'notafruit3',
      },
    };
    const fruits = extractFruits(data);
    expect(fruits).toEqual([
      {
        isFruit: true,
        name: 'apple',
        image: null,
        price: null,
        color: null,
        description: null,
        taste: null,
        expires: null,
        id: 1,
      },
    ]);
  });
});
