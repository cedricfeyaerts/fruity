import { search } from '@/utils/filters.js';

describe('search', () => {
  it('finds all the objects answering to a condition', () => {
    const data = {
      a: { name: 'a', valid: true },
      b: {
        c: { name: 'c', valid: true },
      },
      d: {
        e: {
          f: { name: 'f', valid: true },
          g: { name: 'g', valid: false },
        },
      },
      h: {
        something: 'else',
      },
    };
    const items = search(data, (a) => a.valid);
    expect(items).toContainEqual({ name: 'a', valid: true });
    expect(items).toContainEqual({ name: 'c', valid: true });
    expect(items).toContainEqual({ name: 'f', valid: true });
    expect(items.length).toBe(3);
  });
});
