import mutations from '@/store/mutations.js';
import getters from '@/store/getters.js';
import actions from '@/store/actions.js';
import Api from '@/utils/api.js';
import { createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

jest.mock('@/utils/api.js');

let storeConfig;
let localVue;
let state;

beforeEach(() => {
  state = {
    fruitById: {
      1: {
        name: 'Im a fruit',
      },
      2: {
        name: 'Im another fruit',
      },
    },
    newFruit: {
      name: null,
    },
  };
  storeConfig = { state, mutations, actions, getters };
  localVue = createLocalVue();
  localVue.use(Vuex);
});

describe('deleteFruit', () => {
  it('removes a fruit from the state.fruitById', async () => {
    Api.delete.mockImplementation(() => {
      return Promise.resolve({
        data: '',
      });
    });
    const store = new Vuex.Store(storeConfig);
    await store.dispatch('deleteFruit', 1).then();

    expect(store.state.fruitById[1]).toBe(undefined);
    expect(store.state.fruitById[2]).not.toBe(undefined);
  });
});

describe('createFruit', () => {
  it('add a fruit if the Api answers the correct data', async () => {
    const aNewFruit = {
      id: 3,
      isFruit: true,
      name: 'a new fruit',
    };
    Api.post.mockImplementation(() => {
      return Promise.resolve({
        data: aNewFruit,
      });
    });
    const store = new Vuex.Store(storeConfig);
    await store.dispatch('createFruit', {}).then();
    expect(store.state.fruitById[1]).not.toBeNull;
    expect(store.state.fruitById[2]).not.toBeNull;
    expect(store.state.fruitById['3'].name).toEqual('a new fruit');
    expect(store.state.newFruit).toEqual({
      isFruit: true,
      name: null,
      taste: null,
      description: null,
      price: null,
      color: null,
      image: null,
    });
  });
});

describe('loadData', () => {
  it('it reset fruits and load some new ones', async () => {
    const aNewFruit = {
      id: 3,
      isFruit: true,
      name: 'a new fruit',
    };
    Api.list.mockImplementation(() => {
      return Promise.resolve({
        data: aNewFruit,
      });
    });
    const store = new Vuex.Store(storeConfig);
    await store.dispatch('loadData', {}).then();
    expect(store.state.fruitById[1]).toBeNull;
    expect(store.state.fruitById[2]).toBeNull;
    expect(store.state.fruitById['3'].name).toEqual('a new fruit');
    expect(store.state.newFruit).toEqual({
      isFruit: true,
      name: null,
      taste: null,
      description: null,
      price: null,
      color: null,
      image: null,
    });
  });
});

describe('updateNewFruit', () => {
  it('update a newFruit value', () => {
    const store = new Vuex.Store(storeConfig);
    store.commit('updateNewFruit', { field: 'name', value: 'some name' });
    expect(store.state.newFruit.name).toEqual('some name');
  });
});

describe('resetNewFruit', () => {
  it('set all newFruit value to null', () => {
    const store = new Vuex.Store(storeConfig);
    store.commit('resetNewFruit');
    expect(store.state.newFruit).toEqual({
      isFruit: true,
      name: null,
      taste: null,
      description: null,
      price: null,
      color: null,
      image: null,
    });
  });
});
