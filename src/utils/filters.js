export function search(data, validate) {
  let items = [];
  if (validate(data)) {
    items = [{ ...data }];
  }
  for (let key in data) {
    let subset = data[key];
    if (typeof subset === 'object' && subset != null) {
      items = [...items, ...search(subset, validate)];
    }
    if (Array.isArray(subset)) {
      for (let i of subset) {
        items = [...items, ...search(i, validate)];
      }
    }
  }
  return items;
}
