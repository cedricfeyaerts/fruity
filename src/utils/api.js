import Axios from 'axios';

export default {
  get(baseUrl, id) {
    let url = baseUrl;
    if (id) {
      url += '/' + id;
    }
    return Axios.get(url);
  },

  list(baseUrl) {
    return Axios.get(baseUrl);
  },

  post(baseUrl, payload) {
    return Axios.post(baseUrl, payload);
  },

  delete(baseUrl, id) {
    let url = baseUrl;
    if (id) {
      url += '/' + id;
    }
    return Axios.delete(url);
  },
};
