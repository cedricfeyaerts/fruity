import { search } from '@/utils/filters.js';

export function extractFruits(data) {
  let rawFruits = search(data, isFruitValid);
  return rawFruits.map(extractFruit);
}

function isFruitValid(rawFruit) {
  return (
    typeof rawFruit.isFruit !== 'undefined' &&
    typeof rawFruit.name === 'string' &&
    typeof rawFruit.id === 'number'
  );
}

export function extractFruit(raw) {
  let { name, image, price, color, description, taste, expires, id } = raw;
  let fruit = {
    isFruit: true,
    name,
    image,
    price,
    color,
    description,
    taste,
    expires,
    id,
  };
  for (let key in fruit) {
    if (typeof fruit[key] === 'undefined') {
      fruit[key] = null;
    }
  }
  return fruit;
}

export function createEmptyFruit() {
  return {
    isFruit: true,
    name: null,
    image: null,
    price: null,
    color: null,
    description: null,
    taste: null,
  };
}
