import Vuex from 'vuex';
import Vue from 'vue';
import mutations from './mutations.js';
import getters from './getters.js';
import actions from './actions.js';

Vue.use(Vuex);

const state = {
  loading: false,
  apiUrl: 'http://127.0.0.1:3000/fruit',
  newFruit: {},
  fruitById: {},
};

export default new Vuex.Store({
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions,
});
