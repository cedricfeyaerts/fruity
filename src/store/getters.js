export default {
  orderedFruits: (state) => {
    return Object.values(state.fruitById);
  },
};
