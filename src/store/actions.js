import Api from '@/utils/api.js';

export default {
  loadData({ state, commit }) {
    commit('setLoading', true);
    commit('resetFruits');
    commit('resetNewFruit');
    Api.list(state.apiUrl)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        // handle error
        console.log('error', error);
        return {};
      })
      .then((data) => {
        commit('setLoading', false);
        commit('addFruits', data);
      });
  },
  createFruit({ state, commit, dispatch }, payload) {
    commit('setLoading', true);
    Api.post(state.apiUrl, payload)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        // handle error
        console.log('error', error);
      })
      .then((data) => {
        commit('setLoading', false);
        commit('addFruits', data);
        commit('resetNewFruit');
        return data;
      })
      .then((data) => {
        dispatch('scrollToFruit', data.id);
      });
  },
  deleteFruit({ state, commit }, id) {
    commit('setLoading', true);
    Api.delete(state.apiUrl, id)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        // handle error
        console.log('error', error);
      })
      .then(() => {
        commit('setLoading', false);
        commit('removeFruits', id);
      });
  },

  scrollToFruit(_, id) {
    const fruitElement = document.querySelector('#fruit' + id);
    if (fruitElement) {
      document.querySelector('#fruit' + id).scrollIntoView();
    }
  },
};
