import { extractFruits, createEmptyFruit } from '@/utils/fruit-api.js';

export default {
  setLoading(state, payload) {
    state.loading = !!payload;
  },
  resetFruits(state) {
    state.fruitById = {};
  },
  addFruits(state, data) {
    let fruits = extractFruits(data);
    fruits = fruits.reduce((map, obj) => {
      map[obj.id] = obj;
      return map;
    }, {});
    state.fruitById = { ...state.fruitById, ...fruits };
  },
  removeFruits(state, id) {
    let fruitById = { ...state.fruitById };
    delete fruitById[id];
    state.fruitById = fruitById;
  },
  updateNewFruit(state, payload) {
    let { field, value } = payload;
    state.newFruit[field] = value;
  },
  resetNewFruit(state) {
    state.newFruit = createEmptyFruit();
  },
};
